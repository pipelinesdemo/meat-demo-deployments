echo "Starting Tests"

description() {
	echo -ne "$* \t" > description.txt
}

run_test() {
	local func="$1"
	local output=

	# I/O Redirection lets us store all function STDOUT/STDERR except the description call
	# We want the test description to be printed right away.
	if output="$(eval "$func" > output.txt 2>&1)"; then
		echo -e "$(cat description.txt) [PASS]"
		return 0
	else
		# Quiet if successful, verbose for failure
		echo -e "$(cat description.txt) [FAIL]"
		cat output.txt
		return 1
	fi
}

test_fail() {
	description "this is a failing test"
	echo "some stdout"
	echo "some stderr" 1>&2
	false
}

test_pass() {
	description "this test passes"
	echo "some stdout"
	echo "some stderr" 1>&2
	true
}

run_test test_fail
run_test test_pass