###if [ -z $PS1 ] # no prompt?
if [ -v PS1 ]   # On Bash 4.2+ ...
then
  # non-interactive
  echo "non-interactive."
else
  # interactive
  echo "interactive"
fi